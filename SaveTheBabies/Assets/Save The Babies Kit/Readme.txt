Bouncing Babies originally was developed by Dave Baskin in 1984. 
Using this kit project you can rapidly learn how to create such kind of games. 
This project contains all required assets: textures, animations, prefabs etc.
-----------------------------------------------------------------------------

Project supports following target platforms only:
* PC, Mac & Linux Standalone
* Android
* iOS

To start using Save The Babies Kit:
- Choose Save The Babies Kit -> Debug-> Run from main menu
- Press Ctrl+5
- Open GameplayScene scene from "Assets/Save The Babies Kit/Scenes" folder and press Play button.
- Open StartupScene scene from "Assets/Save The Babies Kit/Scenes" folder and press Play button.

"Assets/Save The Babies Kit/Documentation.pdf" contains detailed documentation for this project.

For better look and feel set 16:9 aspect ratio.