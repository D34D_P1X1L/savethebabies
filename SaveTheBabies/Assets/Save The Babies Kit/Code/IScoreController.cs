﻿using UnityEngine;
using System.Collections;

public interface IScoreController
{
    int BabyArrivedToAmbulance();

    bool BabyFellToGround();

    void WaveStarted(int waveNumber);

    void Reset();
}
