﻿using UnityEngine;
using System.Collections;

public class ObjectRespowner : MonoBehaviour
{
    [SerializeField]
    private GameObject _objectForRespown;

    [SerializeField]
    private Transform _respownPoint;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider == null || (collider.gameObject == null)) return;
        var rb = _objectForRespown.GetComponent<Rigidbody2D>();
        if (rb == null) return;
        rb.velocity = Vector2.zero;
        collider.gameObject.transform.position = _respownPoint.position;
    }
}
