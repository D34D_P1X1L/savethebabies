﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;

public class BouncingObjectCatcher : MonoBehaviour
{
    #region Editor properties

    [SerializeField]
    private InputManager _inputManager;

    [SerializeField]
    private BouncingChain _bouncingChain;
    
    [SerializeField]
    private string _babyObjectTag = "Baby";

    [SerializeField]
    private float _minStayAtPointDelay = 0.2f;
    
    #endregion

    #region Fields

    private Collider2D _collider2D;

    private Animator _animatorFirst;

    private Animator _animatorSecond;

    private int _currentBouncingPointIndex = 0;

    private int _maxChainIndex = 0;

    private bool _inMotion = false;

    private AudioSource _as;

    #endregion

    #region Consts

    private const float HorizontalNegativeDetection = -0.9f;

    private const float HorizontalPositiveDetection = 0.9f;

    #endregion

    #region Properties

    private bool IsInMotion
    {
        get { return _inMotion; }
        set
        {
            _inMotion = value;
            if (_animatorFirst != null && (_animatorSecond != null))
            {
                _animatorFirst.SetBool("IsInMotion", _inMotion);
                _animatorSecond.SetBool("IsInMotion", _inMotion);
            }
        }
    }

    #endregion

    #region Methods

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider == null || (collider.gameObject == null)) return;
        if (!collider.CompareTag(_babyObjectTag)) return;
        DoBounceBaby(collider);
        PlayBounceSound();
    }

    private void PlayBounceSound()
    {
        if (_as == null) return;
        _as.Play();
    }

    private void DoBounceBaby(Collider2D baby)
    {
        var rb = baby.GetComponent<Rigidbody2D>();
        if (rb == null) return;
        var bp = _bouncingChain.GetBouncingPoint(_currentBouncingPointIndex);
        if (bp == null) return;
        baby.gameObject.transform.position = bp.BounceHitPosition;
        rb.velocity = bp.AngularVelocity;
    }

    private void Awake()
    {
        _collider2D = gameObject.GetComponent<Collider2D>();
        _as = gameObject.GetComponent<AudioSource>();

        var childAnimators = gameObject.GetComponentsInChildren<Animator>();
        if (childAnimators != null && (childAnimators.Length == 2))
        {
            _animatorFirst = childAnimators[0];
            _animatorSecond = childAnimators[1];
        }

        var exists = CheckBouncingChainPresents();
        
        if (!exists)
        {
            Debug.LogErrorFormat("There is bouncing Object catcher ({0}) without Bouncing chain.", gameObject.name);
        }
        gameObject.SetActive(exists);
    }

    private void OnEnable()
    {
        var exists = CheckBouncingChainPresents();
        if (!exists)
        {
            Debug.LogErrorFormat("There is bouncing Object catcher ({0}) without Bouncing chain.", gameObject.name);
            gameObject.SetActive(false);
        }
    }

    private void Start()
    {
        GameplayController.Current.GameStarted += OnGameStarted;
        GameplayController.Current.GameIsFinished += OnGameIsFinished;
        GameplayController.Current.GameIsOver += OnGameIsOver;

        _maxChainIndex = GetMaxChainIndex();
        CheckBouncingChainPointsCount();
        SetToBouncingHitPoint(_currentBouncingPointIndex);
    }

    private void FixedUpdate()
    {
        MoveCatcherHorizontaly();
    }

    private void MoveCatcherHorizontaly()
    {
        if (IsInMotion) return;
        var h = _inputManager.Horizontal();
        if (h > HorizontalPositiveDetection)
        {
            if (_currentBouncingPointIndex >= _maxChainIndex) return;
            StartCoroutine(MoveToHitPoint(++_currentBouncingPointIndex));
        }
        else if (h < HorizontalNegativeDetection)
        {
            if (_currentBouncingPointIndex <= 0) return;
            StartCoroutine(MoveToHitPoint(--_currentBouncingPointIndex));
        }
    }

    private IEnumerator MoveToHitPoint(int index)
    {
        var nextPoint = _bouncingChain.GetBouncingPoint(index);
        if (nextPoint != null)
        {
            // Collider should be disabled during movement
            _collider2D.enabled = false;
            IsInMotion = true;
            var distance = (nextPoint.BounceHitPosition - gameObject.transform.position).magnitude;
            const int movementSteps = 10;
            var movementIterations = distance/movementSteps;
            for (var i = 0; i < movementSteps; i++)
            {
                gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, nextPoint.BounceHitPosition, movementIterations);
                yield return null;
            }
            gameObject.transform.position = nextPoint.BounceHitPosition;
            _collider2D.enabled = true;
            yield return new WaitForSeconds(_minStayAtPointDelay);
            IsInMotion = false;
        }
    }
    
    private int GetMaxChainIndex()
    {
        if (_bouncingChain == null) return 0;
        return _bouncingChain.BouncingPointsCount - 1;
    }

    private bool CheckBouncingChainPresents()
    {
        return _bouncingChain != null;
    }

    private void CheckBouncingChainPointsCount()
    {
        if (_bouncingChain == null) return;
        if (_bouncingChain.BouncingPointsCount <= 0)
        {
            Debug.LogWarningFormat("There are no bouncing points defined in bouncing chain {0}", _bouncingChain.gameObject.name);
        }
    }

    private void SetToBouncingHitPoint(int index)
    {
        if (_bouncingChain == null || (_bouncingChain.BouncingPointsCount == 0)) return;
        var point = _bouncingChain.GetBouncingPoint(index);
        if (point == null) return;
        gameObject.transform.position = point.BounceHitPosition;
    }

    private void OnGameStarted(object sender, EventArgs e)
    {
        enabled = true;
        _currentBouncingPointIndex = 0;
        IsInMotion = false;
        SetToBouncingHitPoint(_currentBouncingPointIndex);
    }

    private void OnGameIsFinished(object sender, EventArgs e)
    {
        enabled = false;
    }

    private void OnGameIsOver(object sender, EventArgs e)
    {
        enabled = false;
    }

    #endregion
}
