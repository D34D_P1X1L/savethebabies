﻿using System;
using UnityEngine;

[Serializable]
public class BouncingPoint : MonoBehaviour 
{
    #region Constants

    public const float MinBounceMaxHeight = 0.5f;

    public const float MaxBounceMaxHeight = 5.0f;

    public const float MinBounceDistance = 1f;

    public const float MaxBounceDistance = 5.0f;

    #endregion

    #region Editor properties

    [SerializeField]
    [Range(MinBounceMaxHeight, MaxBounceMaxHeight)]
    private float _bounceMaxHeight = MinBounceMaxHeight;

    [SerializeField]
    [Range(MinBounceDistance, MaxBounceDistance)]
    private float _bounceDistance = MinBounceDistance;

    #endregion

    #region Fields
    
    [HideInInspector]
    [SerializeField]
    private BouncingPoint _nextPoint;

    #endregion

    #region Methods

    public void ParentToChain(Transform parent)
    {
        gameObject.transform.SetParent(parent);
    }

    public void SetNextPoint(BouncingPoint point)
    {
        _nextPoint = point;
    }

    public Vector3 CalculateVelocity(float maxHeight, float distance)
    {
        var verticalSpeed = Mathf.Sqrt(2 * Gravity * BounceMaxHeight);
        var flightTotalTime = 2 * verticalSpeed / Gravity;
        var horizontalSpeed = BounceDistance / flightTotalTime;
        return new Vector3(horizontalSpeed, verticalSpeed);
    }

    public void SetBounceMaxHeight(float maxHeight)
    {
        if (maxHeight < MinBounceMaxHeight || (maxHeight > MaxBounceMaxHeight)) return;
        _bounceMaxHeight = maxHeight;
    }

    public void SetBounceDistance(float bounceDistance)
    {
        if (bounceDistance < 0 || (bounceDistance < MinBounceDistance) || (bounceDistance > MaxBounceDistance)) return;
        _bounceDistance = bounceDistance;
        if (_nextPoint == null) return;
        var nextPosition = _nextPoint.transform.position;
        nextPosition.x = gameObject.transform.position.x + _bounceDistance;
        _nextPoint.transform.position = nextPosition;
    }
    
    #endregion

    #region Properties

    public Vector3 NextPointAddingPosition
    {
        get 
        {
            var position = BounceHitPosition;
            position.x += BounceDistance;
            return position;
        }
    }

    public float BounceMaxHeight
    {
        get { return _bounceMaxHeight; }
        set 
        {
            _bounceMaxHeight = value;
        }
    }

    public float BounceDistance
    {
        get { return _bounceDistance; }
        set 
        {
            _bounceDistance = value;
        }
    }

    public Vector3 AngularVelocity
    {
        get { return CalculateVelocity(BounceMaxHeight, BounceDistance); }
    }

    public Vector3 BounceHitPosition
    {
        get { return gameObject.transform.position; } 
    }

    public float Gravity
    {
        get { return Physics.gravity.magnitude; }
    }

    #endregion
}
