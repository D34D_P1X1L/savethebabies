﻿using System.Collections;
using UnityEngine;

public class BabySpawnPoint : MonoBehaviour 
{
    #region Consts

    private const int MinPooledBabies = 10;

    private const int MaxPooledBabies = 20;

    private const float MinHorizontalVelocity = 0.5f;

    private const float MaxHorizontalVelocity = 5f;

    #endregion

    #region Fields

    [SerializeField]
    private GameObject _babyPrefab;
    
    [SerializeField]
    [Range(MinPooledBabies, MaxPooledBabies)]
    private int _pooledBabiesCount = MinPooledBabies;

    [SerializeField]
    [Range(MinHorizontalVelocity, MaxHorizontalVelocity)]
    private float _horizontalVelocity = MinHorizontalVelocity;

    private ObjectPool<Baby> _babiesPool;

    private Coroutine _lastSpawningCoroutine;

    private Wave _currentWave;

    private AudioSource _as;

    #endregion

    #region Methods

    private void Awake()
    {
        _babiesPool = new ObjectPool<Baby>(_babyPrefab, gameObject.transform, _pooledBabiesCount, true);
        _as = gameObject.GetComponent<AudioSource>();
    }
    
    public void BeginSpawning(Wave wave, int waveIndex)
    {
        _lastSpawningCoroutine = StartCoroutine(SpawningCoroutine(wave.MinSpownedBabies, wave.MaxSpownedBabies, wave.DelayBetweenBabies, wave.DelayToNextBabies));
        ScoreController.Current.WaveStarted(waveIndex + 1);
    }

    public void StopSpawning()
    {
        if (_lastSpawningCoroutine == null) return;
        StopCoroutine(_lastSpawningCoroutine);
        _lastSpawningCoroutine = null;
    }

    private IEnumerator SpawningCoroutine(int minBabies, int maxBabies, float delayBetweenBabies, float delayToNextBabies)
    {
        while (true)
        {
            var babiesNumber = Random.Range(minBabies, maxBabies);
            for (var i = 0; i < babiesNumber; i++)
            {
                var bb = _babiesPool.Retrieve();
                if (bb == null)
                {
                    Debug.LogWarningFormat("There is lack of instances in object pool ({0})", gameObject.name);
                }
                else
                {
                    bb.FlyFrom(gameObject.transform.position, _horizontalVelocity);
                    if (_as != null)
                    {
                        _as.Play();
                    }
                }
                yield return new WaitForSeconds(delayBetweenBabies);
            }
            yield return new WaitForSeconds(delayToNextBabies);
        }
    }
    
    public void ReturnBaby(Baby baby)
    {
        _babiesPool.Return(baby);
    }

    #endregion
}
