﻿using UnityEngine;
using System.Collections;

public class Baby : MonoBehaviour 
{
    #region Fields

    private Rigidbody2D _rb;

    private Animator _animator;

    #endregion

    #region Methods

    private void Awake()
    {
        _rb = gameObject.GetComponent<Rigidbody2D>();
        _animator = gameObject.GetComponent<Animator>();
    }

    public void FlyFrom(Vector3 flyFromPosition, float horizontalVelocity)
    {
        _animator.speed = Random.Range(0.4f, 1.0f);
        gameObject.transform.rotation = Quaternion.identity;
        gameObject.SetActive(true);
        gameObject.transform.position = flyFromPosition;
        _rb.velocity = new Vector2(horizontalVelocity, 0);
    }

    #endregion
}
