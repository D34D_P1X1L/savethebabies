﻿using UnityEngine;
using System.Collections;

public class Wave
{
    public Wave(int minSponwedBabies, int maxSpownedBabies, float delayBetweenBabies, float delayToNextBabies, int toNextLevel)
    {
        MinSpownedBabies = minSponwedBabies;
        MaxSpownedBabies = maxSpownedBabies;
        DelayBetweenBabies = delayBetweenBabies;
        DelayToNextBabies = delayToNextBabies;
        BabiesToNextWave = toNextLevel;
    }

    public int MinSpownedBabies { private set; get; }
    public int MaxSpownedBabies { private set; get; }
    public float DelayBetweenBabies { private set; get; }
    public float DelayToNextBabies { private set; get; }
    public int BabiesToNextWave { private set; get; }
}
