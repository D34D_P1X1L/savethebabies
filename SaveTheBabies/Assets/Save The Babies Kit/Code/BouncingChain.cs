﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BouncingChain : MonoBehaviour 
{
    #region Fields

    [HideInInspector]
    [SerializeField]
    private List<BouncingPoint> _bouncingPoints
        = new List<BouncingPoint>();

    [HideInInspector]
    [SerializeField]
    private BouncingPoint _last;

    [SerializeField]
    private string _newPointNameFormat = "BouncingPoint_";

    #endregion

    public void AddNewBouncingPointToEnd()
    {
        var newPoint = CreateNewBouncingPoint(_bouncingPoints.Count);
        newPoint.ParentToChain(gameObject.transform);

        if (_last == null && (_bouncingPoints.Count == 0))
        {
            newPoint.transform.position = gameObject.transform.position;
            _last = newPoint;
        }
        else
        {
            newPoint.transform.position = _last.NextPointAddingPosition;
            _last.SetNextPoint(newPoint);
            _last = newPoint;
        }
        _bouncingPoints.Add(newPoint);
    }

    public void RemoveLastBouncingPoint()
    {
        if (_bouncingPoints == null || (_bouncingPoints.Count <= 0)) return;
        var lastIndex = _bouncingPoints.Count - 1;
        var lastPoint = _bouncingPoints[lastIndex];
        _bouncingPoints.RemoveAt(lastIndex);
        DestroyImmediate(lastPoint.gameObject);
        if (_bouncingPoints.Count <= 0)
        {
            _last = null;
        }
        else
        {
            lastIndex = _bouncingPoints.Count - 1;
            _last = _bouncingPoints[lastIndex];
        }
    }

    private BouncingPoint CreateNewBouncingPoint(int pointNumber)
    {
        var go = new GameObject(_newPointNameFormat + pointNumber, typeof(BouncingPoint));
        var bp = go.GetComponent<BouncingPoint>();
        return bp;
    }

    public BouncingPoint GetBouncingPoint(int index)
    {
        if (index >= _bouncingPoints.Count) return default(BouncingPoint);
        return _bouncingPoints[index];
    }

    #region Properties

    public int BouncingPointsCount
    {
        get 
        {
            var pointsCount = _bouncingPoints == null ? 0 : _bouncingPoints.Count;
            return pointsCount;
        }
    }

    #endregion
}
