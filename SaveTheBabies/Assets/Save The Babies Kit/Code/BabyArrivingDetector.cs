﻿using System;
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2D))]
public class BabyArrivingDetector : MonoBehaviour 
{
    #region Editor fields

    [SerializeField]
    private string _babyTag = "Baby";

    #endregion

    #region Events

    public event EventHandler<BabyArrivedEventArgs> BabyArrived;

    private void OnBabyArrived(GameObject baby)
    {
        var handler = BabyArrived;
        if (handler == null) return;
        handler(this, new BabyArrivedEventArgs(baby));
    }

    #endregion

    #region Methods

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider == null || (collider.gameObject == null)) return;
        if (!collider.gameObject.CompareTag(_babyTag)) return;
        OnBabyArrived(collider.gameObject);
    }

    #endregion
}
