﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using Framework;

public class GameplayController : Singleton<GameplayController, IGameplayController>, IGameplayController
{
    #region Consts

    private const float MinStartGameplayDelay = 1f;
    private const float MaxStartGameplayDelay = 5f;
    private const float MinDelayBetweenWaves = 2.5f;
    private const float MaxDelayBetweenWaves = 5f;

    #endregion

    #region Editor properties

    [SerializeField]
    [Tooltip("Point where the babies come from")]
    private BabySpawnPoint _babySpawnPoint;

    [SerializeField]
    [Range(MinStartGameplayDelay, MaxStartGameplayDelay)]
    private float _startGameplayDelay = MinStartGameplayDelay;

    [Range(MinDelayBetweenWaves, MaxDelayBetweenWaves)]
    private float _delayBetweenWaves = MinDelayBetweenWaves;

    [SerializeField]
    private BabyArrivingDetector _ambulanceDetector;

    [SerializeField]
    private BabyArrivingDetector _groundDetector;

    [SerializeField]
    private GameObject _gameOverPanel;

    [SerializeField]
    private GameObject _gameCompletePanel;

    [SerializeField]
    private GameObject _hudPanel;

    #endregion

    #region Fields

    private IList<Wave> _wavesSettings;

    private int _currentWaveIndex;

    private Wave _currentWave;

    #endregion

    #region IGameplayController

    #region Events

    public event EventHandler GameStarted;

    private void OnGameStarted()
    {
        FireEvent(GameStarted);
    }

    public event EventHandler GameIsFinished;

    private void OnGameIsFinished()
    {
        FireEvent(GameIsFinished);
    }

    public event EventHandler GameIsOver;

    private void OnGameIsOver()
    {
        FireEvent(GameIsOver);
    }

    private void FireEvent(EventHandler handler)
    {
        if (handler == null) return;
        handler(this, EventArgs.Empty);
    }

    #endregion

    #endregion

    #region Methods

    protected override void Awake()
    {
        base.Awake();
        _wavesSettings = new List<Wave>
        {
                        //  MinBabies   //MaxBabies     //Delay between babies      // Delay for next babies    // Babies for next level
            new Wave(   1,              1,              0f,                         3.5f,                       2),
            new Wave(   1,              3,              0.2f,                       3.5f,                       10),
            new Wave(   2,              3,              0.4f,                       4f,                         20)
        };
        BeginListenToAmbulanceArrivings();
        BeginListenToGroundArriving();
    }

    private void Start()
    {
        BeginGameplay();
    }

    private void BeginGameplay()
    {
        ScoreController.Current.Reset();
        OnGameStarted();
        _gameOverPanel.SetActive(false);
        _gameCompletePanel.SetActive(false);
        _hudPanel.SetActive(true);
        _currentWaveIndex = 0;
        var w = GetWaveByIndex(_currentWaveIndex);
        if (w == null)
        {
            TheGameIsFinished();
            return;
        }
        _currentWave = w;
        StartCoroutine(StartWave(_currentWave, _startGameplayDelay));
    }

    private Wave GetWaveByIndex(int waveIndex)
    {
        var result = default(Wave);
        if (waveIndex < _wavesSettings.Count)
        {
            result = _wavesSettings[waveIndex];
        }
        return result;
    }
    
    private void BeginListenToAmbulanceArrivings()
    {
        _ambulanceDetector.BabyArrived -= OnBabyArrivedToAmbulance;
        _ambulanceDetector.BabyArrived += OnBabyArrivedToAmbulance;
    }
    
    private void BeginListenToGroundArriving()
    {
        _groundDetector.BabyArrived -= OnBabyArrivedToGround;
        _groundDetector.BabyArrived += OnBabyArrivedToGround;
    }

    private void OnBabyArrivedToAmbulance(object sender, BabyArrivedEventArgs e)
    {
        _babySpawnPoint.ReturnBaby(e.Baby);
        var currentScore = ScoreController.Current.BabyArrivedToAmbulance();
        if (currentScore > _currentWave.BabiesToNextWave)
        {
            var w = GetWaveByIndex(_currentWaveIndex + 1);
            if (w == null)
            {
                TheGameIsFinished();
            }
            else
            {
                ++_currentWaveIndex;
                _currentWave = w;
                StartCoroutine(StartWave(_currentWave, _delayBetweenWaves));
            }
        }
    }

    private IEnumerator StartWave(Wave wave, float moveToWaveDelay)
    {
        _babySpawnPoint.StopSpawning();
        yield return new WaitForSeconds(moveToWaveDelay);
        _babySpawnPoint.BeginSpawning(wave, _currentWaveIndex);
    }

    private void OnBabyArrivedToGround(object sender, BabyArrivedEventArgs e)
    {
        _babySpawnPoint.ReturnBaby(e.Baby);
        var isGameOver = ScoreController.Current.BabyFellToGround();
        if (isGameOver)
        {
            TheGameIsOver();
        }
    }

    private void TheGameIsFinished()
    {
        _gameOverPanel.SetActive(false);
        _hudPanel.SetActive(false);
        _gameCompletePanel.SetActive(true);
        _babySpawnPoint.StopSpawning();
        OnGameIsFinished();
    }

    private void TheGameIsOver()
    {
        _gameCompletePanel.SetActive(false);
        _hudPanel.SetActive(false);
        _gameOverPanel.SetActive(true);
        _babySpawnPoint.StopSpawning();
        OnGameIsOver();
    }

    public void RestartLevel()
    {
        BeginGameplay();
    }

    #endregion
}
