﻿using UnityEngine;
using System.Collections;

public class CurcleGeometryHelper
{
    public static Vector3 GetCenterByThreePoints(Vector3 p1, Vector3 p2, Vector3 p3)
    {
        var yDelta_a = p2.y - p1.y;
        var xDelta_a = p2.x - p1.x;
        var yDelta_b = p3.y - p2.y;
        var xDelta_b = p3.x - p2.x;

        var aSlope = yDelta_a / xDelta_a;
        var bSlope = yDelta_b / xDelta_b;

        var center = Vector3.zero;

        if (Mathf.Abs(xDelta_a) <= 0.000000001f && Mathf.Abs(yDelta_b) <= 0.000000001)
        {
            center.x = 0.5f * (p2.x + p3.x);
            center.y = 0.5f * (p1.y + p2.y);
            return center;
        }
        
        center.x = (aSlope * bSlope * (p1.y - p3.y) + bSlope * (p1.x + p2.x)
                    - aSlope * (p2.x + p3.x)) / (2 * (bSlope - aSlope));

        center.y = -1 * (center.x - ((p1.x + p2.x) / 2)) / aSlope + (p1.y + p2.y) / 2;

        return center;
    }
}
