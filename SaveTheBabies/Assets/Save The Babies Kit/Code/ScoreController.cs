﻿using System;
using UnityEngine;
using System.Collections;
using System.Linq;
using Framework;
using UnityEngine.UI;

public class ScoreController : Singleton<ScoreController, IScoreController>, IScoreController
{
    #region Editor Properties

    [SerializeField]
    private Text _scoreText;

    [SerializeField]
    private Text _waveText;

    [SerializeField]
    private Image[] _babiesToGameOver;

    private int _score;

    private int _babiesToLoseLeft;

    #endregion

    #region Methods

    protected override void Awake()
    {
        base.Awake();
    }
    
    #endregion
    
    #region IScoreController

    #region Methods

    public void Reset()
    {
        _scoreText.text = "0";
        _waveText.text = "1";
        _score = 0;
        _babiesToLoseLeft = _babiesToGameOver.Length;
        for (var i = 0; i < _babiesToGameOver.Length; i++)
        {
            _babiesToGameOver[i].gameObject.SetActive(true);
        }
    }

    public void WaveStarted(int waveNumber)
    {
        _waveText.text = waveNumber.ToString();
    }

    public int BabyArrivedToAmbulance()
    {
        _scoreText.text = (++_score).ToString();
        return _score;
    }

    public bool BabyFellToGround()
    {
        if (_babiesToLoseLeft <= 0)
        {
            return true;
        }
        var index = _babiesToLoseLeft - 1;
        _babiesToGameOver[index].gameObject.SetActive(false);
        _babiesToLoseLeft--;
        return false;
    }

    #endregion

    #endregion
}
