﻿using System;
using UnityEngine;
using System.Collections;

public class MobileInputManager : MonoBehaviour , IInputManager
{
    #region Fields

    [SerializeField]
    private GameObject _mobileControlsPanel;

    private float _horizontal;

    #endregion

    #region IInputManager

    public float Horizontal()
    {
        return _horizontal;
    }

    #endregion

    #region Methods

    public void Start()
    {
        GameplayController.Current.GameStarted += OnGameStarted;
        GameplayController.Current.GameIsOver += OnGameIsOver;
        GameplayController.Current.GameIsFinished += OnGameIsFinished;
    }

    private void OnGameIsFinished(object sender, EventArgs e)
    {
        DisableManager();
    }

    private void OnGameIsOver(object sender, EventArgs e)
    {
        DisableManager();
    }

    private void OnGameStarted(object sender, EventArgs e)
    {
        EnableManager();
    }

    public void DisableManager()
    {
        _mobileControlsPanel.SetActive(false);
        gameObject.SetActive(false);
    }

    public void EnableManager()
    {
        _mobileControlsPanel.SetActive(true);
        gameObject.SetActive(true);
    }

    public void SetHorizontalPositive()
    {
        _horizontal = 1;
    }

    public void SetHorizontalNegative()
    {
        _horizontal = -1;
    }

    public void ResetHorizontal()
    {
        _horizontal = 0;
    }

    #endregion
}
