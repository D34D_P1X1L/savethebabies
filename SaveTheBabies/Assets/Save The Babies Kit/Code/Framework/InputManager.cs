﻿#if UNITY_STANDALONE || UNITY_XBOX360 || UNITY_XBOXONE || UNITY_WSA_10_0
#define UNITY_PC_INPUT
#elif UNITY_ANDROID || UNITY_IOS
#define UNITY_MOBILE_INPUT
#endif

using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour 
{
    #region Editor Properties

    [SerializeField]
    private MobileInputManager _mobilInputManager;

    [SerializeField]
    private PcInputManager _pcInputManager;

    #endregion

    #region Fields

    private IInputManager _currentInputManager;

    #endregion

    #region Methods

    private void Awake()
    {
#if UNITY_PC_INPUT
        _currentInputManager = (IInputManager) _pcInputManager;
        _mobilInputManager.DisableManager();
#elif UNITY_MOBILE_INPUT
        _currentInputManager = (IInputManager) _mobilInputManager;
        _pcInputManager.DisableManager();
#endif
    }

    public float Horizontal()
    {
        return _currentInputManager.Horizontal();
    }

    #endregion
}
