﻿using UnityEngine;

namespace Framework
{
    public class Singleton<T, I> : MonoBehaviour
        where T : Component, I, new()
    {
        #region Fields

        protected static I _current;

        #endregion

        #region Methods

        protected virtual void Awake()
        {
            if (_current == null)
            {
                _current = (I)GetComponent<T>();
            }
            else
            {
                Destroy(this);
            }
        }

        private static void CreateGameObjectWithT()
        {
            var go = new GameObject(typeof (T).Name);
            go.AddComponent<T>();
        }

        #endregion
        
        #region Properties

        public static I Current
        {
            get
            {
                if (_current == null)
                {
                    CreateGameObjectWithT();
                }
                return _current;
            }
        }

        #endregion
    }
}