﻿using UnityEngine;
using System.Collections;

public class PcInputManager : MonoBehaviour, IInputManager
{
    #region Editor properties

    [SerializeField]
    private string _horizontalAxisName = "Horizontal";

    #endregion

    #region IInputManager

    public void DisableManager()
    {
        gameObject.SetActive(false);
    }

    public float Horizontal()
    {
        return Input.GetAxisRaw(_horizontalAxisName);
    }

    #endregion
}
