﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityObject = UnityEngine.Object;

public class ObjectPool<T>
    where T : Component
{
    #region Fields

    private readonly GameObject _targetPrefab;

    private int _elementsCount;

    private bool _canGrow;

    private bool _initialized;

    private readonly IList<T> _instancesList;

    private Transform _parent;

    #endregion

    #region Constructors

    public ObjectPool(GameObject targetPrefab, Transform parent, int elementsCount, bool canGrow=false, bool autoinit = true)
    {
        _targetPrefab = targetPrefab;
        _parent = parent;
        _elementsCount = elementsCount;
        _canGrow = canGrow;
        _initialized = false;
        _instancesList = new List<T>();
        if (autoinit)
        {
            Initialize();
        }
    }

    #endregion

    #region Methods

    public void Initialize()
    {
        if (_initialized) return;
        if (_targetPrefab == null)
        {
            Debug.LogErrorFormat("Can't initialize object pool for {0}. Target prefab isn't set", typeof(T).Name);
            return;
        }
        for (var i = 0; i < _elementsCount; i++)
        {
            var instance = UnityObject.Instantiate(_targetPrefab);
            instance.SetActive(false);
            instance.transform.SetParent(_parent);
            var c = instance.GetComponent<T>();
            if (c == null)
            {
                Debug.LogWarningFormat("There is no required by object pool component ({0}) on instantiated object", typeof(T).Name);
                UnityObject.Destroy(instance);
                continue;
            }
            _instancesList.Add(c);
        }
        _initialized = true;
    }

    public T Retrieve()
    {
        var result = default(T);
        for (var i = 0; i < _instancesList.Count; i++)
        {
            if (!_instancesList[i].gameObject.activeSelf)
            {
                result = _instancesList[i];
                break;
            }
        }
        if (result == null && (_canGrow))
        {
            var instance = UnityObject.Instantiate(_targetPrefab);
            instance.SetActive(false);
            result = instance.GetComponent<T>();
            if (result == null)
            {
                Debug.LogWarningFormat("There is no required by object pool component ({0}) on instantiated object",
                    typeof (T).Name);
                UnityObject.Destroy(instance);
            }
            else
            {
                _instancesList.Add(result);
            }

        }
        return result;
    }

    public void Return(T obj)
    {
        obj.gameObject.SetActive(false);
    }

    #endregion
}
