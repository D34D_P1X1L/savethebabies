﻿using UnityEngine;

public interface IInputManager
{
    #region Methods

    float Horizontal();

    void DisableManager();

    #endregion
}
