﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GoToSceneWithFade : MonoBehaviour 
{
    #region Editor properties

    public Image FadeInOutImage;

    public GameObject LoadingText;

    public GameObject[] DeleteWhenShaded;

    public string GoToSceneName = "GameplayScene";

    [SerializeField]
    private float _stayFadedTimeDelay = 1.0f;

    #endregion

    #region Methods

    public void GoToScene()
    {
        FadeInOutImage.raycastTarget = true;
        DontDestroyOnLoad(gameObject);
        StartCoroutine(GoToSceneInrnal(GoToSceneName));
    }

    private IEnumerator GoToSceneInrnal(string sceneName)
    {
        yield return StartCoroutine(FadeInImageCoroutine(0.1f, FadeInOutImage));
        LoadingText.SetActive(true);
        yield return null;
        DeleteUnnecessaryObjects();
        yield return new WaitForSeconds(_stayFadedTimeDelay);
        yield return StartCoroutine(NavigateToScene(sceneName));
        LoadingText.SetActive(false);
        yield return StartCoroutine(FadeOutImageCoroutine(0.1f, FadeInOutImage));
        Destroy(gameObject);
    }

    private void DeleteUnnecessaryObjects()
    {
        for (var i = 0; i < DeleteWhenShaded.Length; i++)
        {
            Destroy(DeleteWhenShaded[i]);
        }
    }

    private IEnumerator NavigateToScene(string sceneName)
    {
        var op = Application.LoadLevelAsync(sceneName);
        while (!op.isDone)
        {
            yield return null;
        }
    }

    private IEnumerator FadeInImageCoroutine(float alphaIncrement, Image image)
    {
        while (image.color.a < 1)
        {
            var c = image.color;
            c.a += alphaIncrement;
            image.color = c;
            yield return null;
        }
    }

    private IEnumerator FadeOutImageCoroutine(float alphaIncrement, Image image)
    {
        while (image.color.a > 0)
        {
            var c = image.color;
            c.a -= alphaIncrement;
            image.color = c;
            yield return null;
        }
    }

    #endregion
}
