﻿using System;
using UnityEngine;
using System.Collections;

public class BabyArrivedEventArgs : EventArgs
{
    public BabyArrivedEventArgs(GameObject gameObject)
    {
        Baby = gameObject.GetComponent<Baby>();
    }

    public Baby Baby { get; private set; }
}
