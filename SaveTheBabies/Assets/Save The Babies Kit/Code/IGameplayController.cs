﻿using System;
using UnityEngine;
using System.Collections;

public interface IGameplayController 
{
    #region Events

    event EventHandler GameStarted;

    event EventHandler GameIsFinished;

    event EventHandler GameIsOver;

    #endregion
}