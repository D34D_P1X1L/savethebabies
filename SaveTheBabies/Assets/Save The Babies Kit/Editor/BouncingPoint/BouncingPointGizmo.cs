﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class BouncingPointGizmo
{
    #region Methods

    [DrawGizmo(GizmoType.NonSelected | GizmoType.NotInSelectionHierarchy)]
    public static void DrawBouncingPointGizmo(BouncingPoint point, GizmoType gizmo)
    {
        var oldColor = Handles.color;
        var oldMatrix = Handles.matrix;

        DrawBouncingPointGizmoInternal(point);

        Handles.color = oldColor;
        Handles.matrix = oldMatrix;
    }

    private static void DrawBouncingPointGizmoInternal(BouncingPoint point)
    {
        BouncingPointGizmoHelper.DrawBounceHitPosition(point, Color.gray);
        BouncingPointGizmoHelper.DrawBounceMaxHeight(point, Color.white);
        BouncingPointGizmoHelper.DrawDescentPoint(point, Color.gray);
        BouncingPointGizmoHelper.DrawVelocityVector(point, Color.gray);
    }
    
    #endregion
}
