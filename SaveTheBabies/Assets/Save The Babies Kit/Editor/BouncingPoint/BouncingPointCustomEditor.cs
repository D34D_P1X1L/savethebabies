﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(BouncingPoint))]
public class BouncingPointCustomEditor : Editor
{
    #region Fields

    private BouncingPoint _target;
    
    #endregion

    #region Methods

    private void OnEnable()
    {
        _target = (BouncingPoint)target;
    }

    private void OnSceneGUI()
    {
        var oldColor = Handles.color;
        var oldMatrix = Handles.matrix;
        BouncingPointGizmoHelper.DrawBounceHitPosition(_target, Color.blue);
        BouncingPointGizmoHelper.DrawBounceMaxHeight(_target, Color.green);
        BouncingPointGizmoHelper.DrawDescentPoint(_target, Color.green);
        BouncingPointGizmoHelper.DrawVelocityVector(_target, Color.blue);
        Handles.color = oldColor;
        Handles.matrix = oldMatrix;
    }
    
    #endregion
}
