﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(BouncingChain))]
public class BouncingChainCustomEditor : Editor 
{
    #region Fields

    private BouncingChain _target;

    #endregion

    #region Methods

    public void OnEnable()
    {
        _target = (BouncingChain)target;
    }

    public override void OnInspectorGUI()
    {
        DrawBouncingPointsCount();
        DrawIncrementDecrementPointsCount();
        if (GUI.changed)
        {
            EditorUtility.SetDirty(_target);
        }
    }

    private void DrawBouncingPointsCount()
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Points: ", EditorStyles.boldLabel);
        EditorGUILayout.LabelField(_target.BouncingPointsCount.ToString());
        EditorGUILayout.EndHorizontal();
    }

    private void DrawIncrementDecrementPointsCount()
    {
        EditorGUILayout.BeginHorizontal();
        var increment = GUILayout.Button("+", EditorStyles.miniButtonLeft);
        if (increment)
        {
            _target.AddNewBouncingPointToEnd();
        }
        var decrement = GUILayout.Button("-", EditorStyles.miniButtonRight);
        if (decrement)
        {
            _target.RemoveLastBouncingPoint();
        }
        EditorGUILayout.EndHorizontal();
    }

    #endregion
}
