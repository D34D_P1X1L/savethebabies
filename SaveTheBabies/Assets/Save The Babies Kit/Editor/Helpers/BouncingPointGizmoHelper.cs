﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class BouncingPointGizmoHelper 
{
    #region Methods

    public static void DrawBounceHitPosition(BouncingPoint target, Color handleColor)
    {
        Handles.color = handleColor;
        Handles.DotCap(0, target.BounceHitPosition, Quaternion.identity, 0.1f);
    }

    public static Vector3 DrawBounceMaxHeight(BouncingPoint target, Color handleColor)
    {
        Handles.color = handleColor;
        var maxHeightCapPositionX = target.BounceHitPosition.x + (target.BounceDistance / 2);
        var maxHeightCapPositionY = target.BounceHitPosition.y + target.BounceMaxHeight;
        var maxHeightCapPosition = new Vector3(maxHeightCapPositionX, maxHeightCapPositionY, 0);
        var newHeight = Handles.FreeMoveHandle(maxHeightCapPosition, Quaternion.identity, 0.1f, Vector3.zero, Handles.DotCap);
        newHeight = newHeight - target.BounceHitPosition;
        target.SetBounceMaxHeight(newHeight.y);
        return new Vector3(maxHeightCapPositionX, newHeight.y, target.BounceHitPosition.z);
    }

    public static Vector3 DrawDescentPoint(BouncingPoint target, Color handleColor)
    {
        Handles.color = handleColor;
        var descentPointPosition = target.BounceHitPosition;
        descentPointPosition.x += target.BounceDistance;
        var newDistance = Handles.FreeMoveHandle(descentPointPosition, Quaternion.identity, 0.1f, Vector3.zero, Handles.DotCap);
        newDistance = newDistance - target.BounceHitPosition;
        target.SetBounceDistance(newDistance.x);
        return new Vector3(newDistance.x, target.BounceHitPosition.y, target.BounceHitPosition.z);
    }

    public static void DrawVelocityVector(BouncingPoint target, Color vectorColor)
    {
        var velocityVector = target.CalculateVelocity(target.BounceMaxHeight, target.BounceDistance);
        Handles.color = vectorColor;
        var descentPointPosition = target.BounceHitPosition;
        descentPointPosition.x += target.BounceDistance;
        var rotation = Quaternion.LookRotation(velocityVector);
        Handles.ArrowCap(0, target.BounceHitPosition, rotation, 1f);
    }

    //private void DrawTrajectoryArc(Vector3 maxHeightPoint, Vector3 descentPoint, Color arcColor)
    //{
    //    var arcCenter = _target.BounceHitPosition;
    //    arcCenter.x = maxHeightPoint.x;

    //    Handles.color = arcColor;
    //    var trajectoryCircleCenter = CurcleGeometryHelper.GetCenterByThreePoints(_target.BounceHitPosition, maxHeightPoint, descentPoint);
    //    var leftPointVector = _target.BounceHitPosition;
    //    var rightPointVector = descentPoint;
    //    var angle = Vector3.Angle(rightPointVector, leftPointVector);
    //    var radius = (trajectoryCircleCenter - leftPointVector).magnitude;
    //    DrawTrajectory(radius, angle, 10);
    //    // Handles.DrawWireArc(trajectoryCircleCenter, Vector3.back, _target.BounceHitPosition, angle, leftPointVector.magnitude);
    //    Handles.CircleCap(0, trajectoryCircleCenter, Quaternion.identity, 0.1f);
    //}

    //private static void DrawTrajectory(float radius, float angle, int trajectoryPointsNumber)
    //{
    //    Handles.color = Color.red;
    //    angle = 90 + (angle / 2);

    //    var angleIteration = angle / trajectoryPointsNumber;

    //    var x = radius * Mathf.Sin(angle * Mathf.Deg2Rad);
    //    var y = radius * Mathf.Cos(angle * Mathf.Deg2Rad);
    //    var point = new Vector3(x, y, 0);

    //    for (var i = 1; i < trajectoryPointsNumber; i++)
    //    {
    //        angle -= angleIteration;

    //        x = radius * Mathf.Sin(angle * Mathf.Deg2Rad);
    //        y = radius * Mathf.Cos(angle * Mathf.Deg2Rad);
    //        point = new Vector3(x, y, 0);

    //        Handles.CircleCap(0, point, Quaternion.identity, 0.1f);
    //    }
    //}

    #endregion
}
