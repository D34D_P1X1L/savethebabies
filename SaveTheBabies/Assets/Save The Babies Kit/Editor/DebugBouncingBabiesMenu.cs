﻿using UnityEngine;
using UnityEditor;

public class DebugSaveTheBabiesKitMenu : MonoBehaviour
{
    #region Methods

    [MenuItem("Save The Babies Kit/Debug/Run %5")]
    public static void DebugGameMenuItem()
    {
        if (EditorApplication.SaveCurrentSceneIfUserWantsTo())
        {
            EditorApplication.SaveScene();
        }

        EditorApplication.OpenScene("Assets/Save The Babies Kit/Scenes/StartupScene.unity");
        EditorApplication.isPlaying = true;
    }

    #endregion
}
